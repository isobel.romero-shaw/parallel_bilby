import sys


def get_cli_args():
    """Tool to get CLI args (also makes testing easier)"""
    return sys.argv[1:]
